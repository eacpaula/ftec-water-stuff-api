import 'reflect-metadata'
import { Field, ObjectType } from 'type-graphql'

@ObjectType()
export default class DefaultResponse {
	@Field({ nullable: true })
	success?: boolean

	@Field({ nullable: true })
	message?: string

	@Field({ nullable: true })
	status?: number
}
