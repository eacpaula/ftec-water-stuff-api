import 'reflect-metadata'
import { ObjectType, Field, ID, GraphQLTimestamp } from 'type-graphql'
import { Media } from '../media/Media'
import { User } from '../user/User'

@ObjectType()
export class WaterRegister {
	@Field(() => ID)
	id: number

	@Field(() => ID, { nullable: true })
	user_id?: number | null

	@Field()
	code: string

	@Field()
	documentation: string

	@Field(() => ID)
	media_user_id: number

	@Field(() => ID)
	media_documentation_frontside_id: number

	@Field(() => ID)
	media_documentation_backside_id: number

	@Field(() => ID)
	media_water_account_id: number

	@Field()
	accept_terms: boolean

	@Field()
	available: boolean

	@Field()
	country: string

	@Field()
	state: string

	@Field()
	city: string

	@Field()
	neighborhood: string

	@Field()
	address: string

	@Field()
	zipcode: string

	@Field()
	cellphone: string

	@Field()
	telephone: string

	@Field(() => ID)
	userCreatedId: number

	@Field(() => Date)
	createdAt: Date

	@Field(() => ID, { nullable: true })
	userUpdatedId?: number | null

	@Field(() => GraphQLTimestamp, { nullable: true })
	updatedAt?: Date | null

	@Field(() => User, { nullable: true })
	createdBy?: User | null

	@Field(() => User, { nullable: true })
	updatedBy?: User | null

	@Field(() => Media, { nullable: true })
	mediaUser?: Media | null

	@Field(() => Media, { nullable: true })
	mediaDocumentationFrontside?: Media | null

	@Field(() => Media, { nullable: true })
	mediaDocumentationBackside?: Media | null

	@Field(() => Media, { nullable: true })
	mediaWaterAccount?: Media | null
}
