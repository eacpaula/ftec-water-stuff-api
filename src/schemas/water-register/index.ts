import 'reflect-metadata'
import {
	Resolver,
	Query,
	Mutation,
	Arg,
	Ctx,
	FieldResolver,
	Root,
	InputType,
	Field,
	GraphQLTimestamp,
	Int,
} from 'type-graphql'
import { User } from '../user/User'
import { Media } from '../media/Media'
import { WaterRegister } from './WaterRegister'
import { Context } from '../../config/context'
import SearchInput from '../common/SearchInput'
import { AuthenticationError } from 'apollo-server-express'

@InputType()
class WaterRegisterInput {
	@Field(() => Int, { nullable: true })
	id?: number

	@Field(() => Int, { nullable: true })
	user_id?: number | null

	@Field()
	code: string

	@Field()
	documentation: string

	@Field(() => Int)
	media_user_id: number

	@Field(() => Int)
	media_documentation_frontside_id: number

	@Field(() => Int)
	media_documentation_backside_id: number

	@Field(() => Int)
	media_water_account_id: number

	@Field()
	accept_terms: boolean

	@Field()
	available: boolean

	@Field()
	country: string

	@Field()
	state: string

	@Field()
	city: string

	@Field()
	neighborhood: string

	@Field()
	address: string

	@Field()
	zipcode: string

	@Field()
	cellphone: string

	@Field()
	telephone: string

	@Field(() => Int, { nullable: true })
	userCreatedId?: number

	@Field(() => Date, { nullable: true })
	createdAt?: Date

	@Field(() => Int, { nullable: true })
	userUpdatedId?: number | null

	@Field(() => GraphQLTimestamp, { nullable: true })
	updatedAt?: Date | null
}

@Resolver(WaterRegister)
export class WaterRegisterResolver {
	@FieldResolver()
	async createdBy(
		@Root() waterRegister: WaterRegister,
		@Ctx() ctx: Context
	): Promise<User | null> {
		return ctx.prisma.waterRegister
			.findUnique({
				where: {
					id: waterRegister.id,
				},
			})
			.createdBy()
	}

	@FieldResolver()
	async updatedBy(
		@Root() waterRegister: WaterRegister,
		@Ctx() ctx: Context
	): Promise<User | null> {
		return ctx.prisma.waterRegister
			.findUnique({
				where: {
					id: waterRegister.id,
				},
			})
			.updatedBy()
	}

	@FieldResolver()
	async mediaUser(
		@Root() waterRegister: WaterRegister,
		@Ctx() ctx: Context
	): Promise<Media | null> {
		return ctx.prisma.waterRegister
			.findUnique({
				where: {
					id: waterRegister.id,
				},
			})
			.mediaUser()
	}

	@FieldResolver()
	async mediaDocumentationFrontside(
		@Root() waterRegister: WaterRegister,
		@Ctx() ctx: Context
	): Promise<Media | null> {
		return ctx.prisma.waterRegister
			.findUnique({
				where: {
					id: waterRegister.id,
				},
			})
			.mediaDocumentationFrontside()
	}

	@FieldResolver()
	async mediaDocumentationBackside(
		@Root() waterRegister: WaterRegister,
		@Ctx() ctx: Context
	): Promise<Media | null> {
		return ctx.prisma.waterRegister
			.findUnique({
				where: {
					id: waterRegister.id,
				},
			})
			.mediaDocumentationBackside()
	}

	@FieldResolver()
	async mediaWaterAccount(
		@Root() waterRegister: WaterRegister,
		@Ctx() ctx: Context
	): Promise<Media | null> {
		return ctx.prisma.waterRegister
			.findUnique({
				where: {
					id: waterRegister.id,
				},
			})
			.mediaWaterAccount()
	}

	@Mutation(() => WaterRegister)
	async addWaterRegister(
		@Arg('data') data: WaterRegisterInput,
		@Ctx() ctx: Context
	): Promise<WaterRegister> {
		if (!ctx.user)
			throw new AuthenticationError(
				'Usuário não logado para realizar a operação solicitada!'
			)

		return ctx.prisma.waterRegister.create({
			data: {
				address: data.address,
				cellphone: data.cellphone,
				city: data.city,
				code: data.code,
				country: data.country,
				documentation: data.documentation,
				neighborhood: data.neighborhood,
				state: data.state,
				telephone: data.telephone,
				zipcode: data.zipcode,
				accept_terms: data.accept_terms,
				available: data.available,
				createdBy: {
					connect: {
						id: data.userCreatedId
							? data.userCreatedId
							: ctx.user.id,
					},
				},
				user: {
					connect: {
						id: data.user_id ? data.user_id : ctx.user.id,
					},
				},
				mediaUser: {
					connect: {
						id: data.media_user_id,
					},
				},
				mediaDocumentationFrontside: {
					connect: {
						id: data.media_documentation_frontside_id,
					},
				},
				mediaDocumentationBackside: {
					connect: {
						id: data.media_documentation_backside_id,
					},
				},
				mediaWaterAccount: {
					connect: {
						id: data.media_water_account_id,
					},
				},
				createdAt: data.createdAt,
			},
		})
	}

	@Mutation(() => WaterRegister)
	async updateWaterRegister(
		@Arg('data') data: WaterRegisterInput,
		@Ctx() ctx: Context
	): Promise<WaterRegister> {
		if (!ctx.user)
			throw new AuthenticationError(
				'Usuário não logado para realizar a operação solicitada!'
			)

		return ctx.prisma.waterRegister.update({
			data: {
				address: data.address,
				cellphone: data.cellphone,
				city: data.city,
				code: data.code,
				country: data.country,
				documentation: data.documentation,
				neighborhood: data.neighborhood,
				state: data.state,
				telephone: data.telephone,
				zipcode: data.zipcode,
				accept_terms: data.accept_terms,
				available: data.available,
				updatedBy: {
					connect: {
						id: data.userUpdatedId
							? data.userUpdatedId
							: ctx.user.id,
					},
				},
				user: {
					connect: {
						id: data.user_id ? data.user_id : ctx.user.id,
					},
				},
				mediaUser: {
					connect: {
						id: data.media_user_id,
					},
				},
				mediaDocumentationFrontside: {
					connect: {
						id: data.media_documentation_frontside_id,
					},
				},
				mediaDocumentationBackside: {
					connect: {
						id: data.media_documentation_backside_id,
					},
				},
				mediaWaterAccount: {
					connect: {
						id: data.media_water_account_id,
					},
				},
				updatedAt: data.updatedAt,
			},
			where: {
				id: data.id,
			},
		})
	}

	@Mutation(() => WaterRegister)
	async removeWaterRegister(
		@Arg('id', () => Int) id: number,
		@Ctx() ctx: Context
	): Promise<WaterRegister> {
		if (!ctx.user)
			throw new AuthenticationError(
				'Usuário não logado para realizar a operação solicitada!'
			)

		return ctx.prisma.waterRegister.delete({
			where: {
				id,
			},
		})
	}

	@Query(() => WaterRegister, { nullable: true })
	async waterRegister(
		@Arg('id', () => Int) id: number,
		@Ctx() ctx: Context
	): Promise<WaterRegister | null> {
		return ctx.prisma.waterRegister.findUnique({
			where: { id: id },
		})
	}

	@Query(() => WaterRegister, { nullable: true })
	async waterRegisterByUser(
		@Ctx() ctx: Context
	): Promise<WaterRegister | null> {
		return ctx.prisma.waterRegister.findFirst({
			where: { user_id: ctx.user.id },
		})
	}

	@Query(() => [WaterRegister], { nullable: true })
	async waterRegisters(
		@Arg('params') params: SearchInput,
		@Ctx() ctx: Context
	): Promise<WaterRegister[]> {
		if (!params.skip && !params.take)
			return ctx.prisma.waterRegister.findMany({
				where: {
					documentation: {
						contains: params.term,
					},
					available: {
						equals: params.available,
					},
				},
			})

		return ctx.prisma.waterRegister.findMany({
			skip: params.skip,
			take: params.take,
			where: {
				documentation: {
					contains: params.term,
				},
				available: {
					equals: params.available,
				},
			},
		})
	}
}
