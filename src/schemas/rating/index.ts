import 'reflect-metadata'
import {
	Resolver,
	Query,
	Mutation,
	Arg,
	Ctx,
	FieldResolver,
	Root,
	InputType,
	Field,
	Int,
	GraphQLTimestamp,
} from 'type-graphql'
import { User } from '../user/User'
import { Rating } from './Rating'
import { Context } from '../../config/context'
import SearchInput from '../common/SearchInput'
import { AuthenticationError } from 'apollo-server-express'
import { Contact } from '../contact/Contact'

@InputType()
class RatingInput {
	@Field(() => Int, { nullable: true })
	contact_id: number

	@Field(() => Int)
	note: number

	@Field(() => String)
	observation: string
}

@Resolver(Rating)
export class RatingResolver {
	@FieldResolver()
	async contact(
		@Root() rating: Rating,
		@Ctx() ctx: Context
	): Promise<Contact | null> {
		return ctx.prisma.rating
			.findUnique({
				where: {
					id: rating.id,
				},
			})
			.contact()
	}

	@FieldResolver()
	async createdBy(
		@Root() rating: Rating,
		@Ctx() ctx: Context
	): Promise<User | null> {
		return ctx.prisma.rating
			.findUnique({
				where: {
					id: rating.id,
				},
			})
			.createdBy()
	}

	@Mutation(() => Rating)
	async addRating(
		@Arg('data') data: RatingInput,
		@Ctx() ctx: Context
	): Promise<Rating> {
		if (!ctx.user)
			throw new AuthenticationError(
				'Usuário não logado para realizar a operação solicitada!'
			)

		const contact = await ctx.prisma.contact.findFirst({
			where: { id: data.contact_id },
		})

		if (!contact) throw new AuthenticationError('Contato não selecionado!')

		return ctx.prisma.rating.create({
			data: {
				observation: data.observation,
				note: data.note,
				contact: {
					connect: {
						id: contact.id,
					},
				},
				createdBy: {
					connect: {
						id: ctx.user.id,
					},
				},
				createdAt: new Date(),
			},
		})
	}

	@Query(() => Rating, { nullable: true })
	async rating(
		@Arg('id', () => Int) id: number,
		@Ctx() ctx: Context
	): Promise<Rating | null> {
		return ctx.prisma.rating.findUnique({
			where: { id: id },
		})
	}

	@Query(() => [Rating], { nullable: true })
	async ratings(
		@Arg('params') params: SearchInput,
		@Ctx() ctx: Context
	): Promise<Rating[]> {
		if (!params.skip && !params.take) return ctx.prisma.rating.findMany()

		return ctx.prisma.rating.findMany({
			skip: params.skip,
			take: params.take,
		})
	}

	@Query(() => [Rating], { nullable: true })
	async ratingsByUser(
		@Arg('params') params: SearchInput,
		@Ctx() ctx: Context
	): Promise<Rating[]> {
		if (!params.skip && !params.take) return ctx.prisma.rating.findMany()

		return ctx.prisma.rating.findMany({
			skip: params.skip,
			take: params.take,
			where: {
				userCreatedId: ctx.user.id,
			},
		})
	}
}
