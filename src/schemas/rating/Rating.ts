import 'reflect-metadata'
import { ObjectType, Field, ID } from 'type-graphql'
import { Contact } from '../contact/Contact'
import { User } from '../user/User'

@ObjectType()
export class Rating {
	@Field(() => ID)
	id: number

	@Field()
	contact_id: number

	@Field(() => Number)
	note: number

	@Field(() => String)
	observation: string

	@Field(() => ID)
	userCreatedId: number

	@Field(() => Date)
	createdAt: Date

	@Field(() => Contact, { nullable: true })
	contact?: Contact | null

	@Field(() => User, { nullable: true })
	createdBy?: User | null
}
