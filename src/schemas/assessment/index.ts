import 'reflect-metadata'
import {
	Resolver,
	Query,
	Mutation,
	Arg,
	Ctx,
	FieldResolver,
	Root,
	InputType,
	Field,
	Int,
} from 'type-graphql'
import { User } from '../user/User'
import { Assessment, AssessmentsLevel } from './Assessment'
import { Context } from '../../config/context'
import SearchInput from '../common/SearchInput'
import { AuthenticationError } from 'apollo-server-express'
import { Contact } from '../contact/Contact'

@InputType()
class AssessmentInput {
	@Field(() => Int, { nullable: true })
	id?: number

	@Field(() => Int)
	contact_id: number

	@Field(() => Int)
	level: number

	@Field()
	observation: string

	@Field(() => Int, { nullable: true })
	userCreatedId?: number

	@Field(() => Date, { nullable: true })
	createdAt?: Date
}

@Resolver(Assessment)
export class AssessmentResolver {
	@FieldResolver()
	async contact(
		@Root() assessment: Assessment,
		@Ctx() ctx: Context
	): Promise<Contact | null> {
		return ctx.prisma.assessments
			.findUnique({
				where: {
					id: assessment.id,
				},
			})
			.contact()
	}

	@FieldResolver()
	async createdBy(
		@Root() assessment: Assessment,
		@Ctx() ctx: Context
	): Promise<User | null> {
		return ctx.prisma.assessments
			.findUnique({
				where: {
					id: assessment.id,
				},
			})
			.createdBy()
	}

	@Mutation(() => Assessment)
	async addAssessment(
		@Arg('data') data: AssessmentInput,
		@Ctx() ctx: Context
	): Promise<Assessment> {
		if (!ctx.user)
			throw new AuthenticationError(
				'Usuário não logado para realizar a operação solicitada!'
			)

		return ctx.prisma.assessments.create({
			data: {
				level: data.level,
				observation: data.observation,
				contact: {
					connect: {
						id: data.contact_id,
					},
				},
				createdBy: {
					connect: {
						id: data.userCreatedId
							? data.userCreatedId
							: ctx.user.id,
					},
				},
				createdAt: data.createdAt,
			},
		})
	}

	@Query(() => Assessment, { nullable: true })
	async assessment(
		@Arg('id', () => Int) id: number,
		@Ctx() ctx: Context
	): Promise<Assessment | null> {
		return ctx.prisma.assessments.findUnique({
			where: { id: id },
		})
	}

	@Query(() => [Assessment], { nullable: true })
	async assessments(
		@Arg('params') params: SearchInput,
		@Ctx() ctx: Context
	): Promise<Assessment[]> {
		if (!params.skip && !params.take)
			return ctx.prisma.assessments.findMany()

		return ctx.prisma.assessments.findMany({
			skip: params.skip,
			take: params.take,
		})
	}
}
