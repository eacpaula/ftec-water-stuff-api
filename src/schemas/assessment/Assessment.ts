import 'reflect-metadata'
import { ObjectType, Field, ID, GraphQLTimestamp, Int } from 'type-graphql'
import { Contact } from '../contact/Contact'
import { User } from '../user/User'

@ObjectType()
export class Assessment {
	@Field(() => ID)
	id: number

	@Field()
	contact_id: number

	@Field(() => Int)
	level: number

	@Field()
	observation: string

	@Field(() => ID)
	userCreatedId: number

	@Field(() => Date)
	createdAt: Date

	@Field(() => Contact, { nullable: true })
	contact?: Contact | null

	@Field(() => User, { nullable: true })
	createdBy?: User | null
}

export enum AssessmentsLevel {
	VERY_UNSATISFIED = 0,
	UNSATISFIED = 1,
	SATISFIED = 2,
	VERY_SATISFIED = 3,
	EXCELLENT = 4,
}
