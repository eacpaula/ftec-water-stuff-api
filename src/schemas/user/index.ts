import 'reflect-metadata'
import {
	Resolver,
	Query,
	Mutation,
	Arg,
	Ctx,
	Int,
	InputType,
	Field,
	GraphQLTimestamp,
} from 'type-graphql'
import * as bcrypt from 'bcrypt'
import * as jwt from 'jsonwebtoken'
import * as securePin from 'secure-pin'
import moment from 'moment'

import { User } from './User'
import { Context } from '../../config/context'
import SearchInput from '../common/SearchInput'
import { AuthenticationError } from 'apollo-server-express'
import sendEmail from '../../helpers/sendEmail'

const BCRYPT_SALT = 10

@InputType()
class SignupUserInput {
	@Field()
	username: string

	@Field()
	email: string

	@Field()
	password: string

	@Field(() => GraphQLTimestamp, { nullable: true })
	createdAt?: Date

	@Field(() => GraphQLTimestamp, { nullable: true })
	updatedAt?: Date
}

@InputType()
class ChangePasswordUserInput {
	@Field()
	username: string

	@Field()
	password_old: string

	@Field()
	password: string

	@Field()
	password_confirmation: string
}

@Resolver(User)
export class UserResolver {
	@Mutation(() => User)
	async addUser(
		@Arg('data') data: SignupUserInput,
		@Ctx() ctx: Context
	): Promise<User> {
		// if (!ctx.user)
		// 	throw new AuthenticationError(
		// 		'Usuário não logado para realizar a operação solicitada!'
		// 	)

		const salt: string = await bcrypt.genSalt(BCRYPT_SALT)
		const hash: string = await bcrypt.hash(data.password, salt)

		if (!hash)
			throw new Error(
				'Houve um erro inesperado tente novamente mais tarde'
			)

		data.password = hash

		return ctx.prisma.user.create({
			data: {
				username: data.username,
				email: data.email,
				password: data.password,
				role: 'USER',
			},
		})
	}

	@Mutation(() => User)
	async changePassword(
		@Arg('data') data: ChangePasswordUserInput,
		@Ctx() ctx: Context
	): Promise<User> {
		if (data.password !== data.password_confirmation)
			throw new Error('A senha não corresponde a confirmação de senha')

		const user = await ctx.prisma.user.findFirst({
			where: { username: data.username },
		})

		if (!user) throw new Error('Usuário ou senha inválido')

		const isMatch = await bcrypt.compare(data.password_old, user.password)

		if (!isMatch) throw new Error('Usuário ou senha inválido')

		const salt: string = await bcrypt.genSalt(BCRYPT_SALT)
		const hash: string = await bcrypt.hash(data.password, salt)

		if (!hash)
			throw new Error(
				'Houve um erro inesperado tente novamente mais tarde'
			)

		data.password = hash

		return ctx.prisma.user.update({
			data: {
				password: hash,
			},
			where: {
				username: data.username,
			},
		})
	}

	@Mutation(() => User)
	async removeUser(
		@Arg('id', () => Int) id: number,
		@Ctx() ctx: Context
	): Promise<User> {
		if (!ctx.user)
			throw new AuthenticationError(
				'Usuário não logado para realizar a operação solicitada!'
			)

		return ctx.prisma.user.delete({
			where: {
				id,
			},
		})
	}

	@Query(() => User, { nullable: true })
	async user(
		@Arg('id', () => Int) id: number,
		@Ctx() ctx: Context
	): Promise<User | null> {
		if (!ctx.user)
			throw new AuthenticationError(
				'Usuário não logado para realizar a operação solicitada!'
			)

		return ctx.prisma.user.findUnique({
			where: { id: id },
		})
	}

	@Query(() => [User], { nullable: true })
	async users(
		@Arg('params') params: SearchInput,
		@Ctx() ctx: Context
	): Promise<User[]> {
		if (!ctx.user)
			throw new AuthenticationError(
				'Usuário não logado para realizar a operação solicitada!'
			)

		return ctx.prisma.user.findMany({
			skip: params.skip,
			take: params.take,
			where: {
				username: {
					contains: params.term,
				},
			},
		})
	}

	@Query(() => String, { nullable: true })
	async login(
		@Arg('username') username: string,
		@Arg('password') password: string,
		@Ctx() ctx: Context
	): Promise<string> {
		const SECRET = <string>process.env.API_SECRET
		const EXPIRATION = <string>process.env.API_TOKEN_EXPIRATION

		const user = await ctx.prisma.user.findFirst({
			where: { username: username },
		})

		if (!user) throw new Error('Usuário ou senha inválido')

		const isMatch = await bcrypt.compare(password, user.password)

		if (!isMatch) throw new Error('Usuário ou senha inválido')

		return jwt.sign(
			{
				id: user.id,
				username: user.username,
				role: user.role,
			},
			SECRET,
			{
				algorithm: 'HS256',
				subject: `${user.id}`,
				expiresIn: EXPIRATION,
			}
		)
	}

	@Query(() => String, { nullable: true })
	async recoveryPasswordSolicitation(
		@Arg('email') email: string,
		@Ctx() ctx: Context
	): Promise<any> {
		const user = await ctx.prisma.user.findFirst({
			where: { email: email },
		})

		if (!user)
			return {
				message:
					'Se o endereço estiver correto, você recebera um e-mail em alguns instantes!',
				success: false,
			}

		const pin = securePin.generatePinSync(4)

		await ctx.prisma.token.create({
			data: {
				token: pin,
				user_id: user.id,
				expireAt: moment().add(1, 'hours').toDate(),
			},
		})

		await sendEmail(
			`Ftec Water Stuff - Solicitação de Recuperação de Senha`,
			`
				<div>
					<h3>Recuperação de Senha</h3>
					<p>Foi solicitado a recuperação de senha em nossa plataforma.</p>
					<p>Caso você tenha gerado esta solicitação utilize o código abaixo para efetuar a recuperação, se não desconsidere este e-mail</p>
					<p><strong>${pin}</strong></p>
				</div>
			`,
			[String(email)]
		)

		return {
			message:
				'Sua solicitação foi processada com sucesso, se você possuir um e-mail valido em nossa plataforma recebera um e-mail em instantes!',
			success: true,
		}
	}

	@Query(() => String, { nullable: true })
	async recoveryPasswordConfirmPin(
		@Arg('pin') pin: string,
		@Ctx() ctx: Context
	): Promise<any> {
		const token = await ctx.prisma.token.findFirst({
			where: {
				token: pin,
				checked: false,
			},
		})

		if (token != null && moment().isAfter(token.expireAt)) {
			await ctx.prisma.token.delete({
				where: {
					id: token.id,
				},
			})

			return {
				message:
					'O pin inserido expirou! Tente realizar outra solicitação!',
				success: false,
			}
		} else if (token && token.checked) {
			await ctx.prisma.token.delete({
				where: {
					id: token.id,
				},
			})

			return {
				message:
					'O pin inserido ja foi utilizado! Tente realizar outra solicitação!',
				success: false,
			}
		} else if (token && !token.checked) {
			await ctx.prisma.token.update({
				data: {
					checked: true,
				},
				where: {
					id: token.id,
				},
			})

			return {
				message: 'O pin inserido foi validado com sucesso!',
				success: true,
			}
		}

		return {
			message:
				'O pin inserido não existe! Tente realizar outra solicitação!',
			success: true,
		}
	}

	@Query(() => String, { nullable: true })
	async recoveryPassword(
		@Arg('pin') pin: string,
		@Arg('password') password: string,
		@Arg('passwordConfirm') passwordConfirm: string,
		@Ctx() ctx: Context
	): Promise<any> {
		const token = await ctx.prisma.token.findFirst({
			where: {
				token: pin,
				checked: false,
			},
		})

		if (token != null && moment().isAfter(token.expireAt)) {
			await ctx.prisma.token.delete({
				where: {
					id: token.id,
				},
			})

			return {
				message:
					'O pin inserido expirou! Você não pode realizar trocar a senha!',
				success: false,
			}
		} else if (token && !token.checked) {
			await ctx.prisma.token.delete({
				where: {
					id: token.id,
				},
			})

			return {
				message:
					'O pin inserido não foi confirmado! Você não pode trocar a senha!',
				success: false,
			}
		} else if (token && token.checked) {
			const user = await ctx.prisma.user.findFirst({
				where: { id: token.user_id },
			})

			await ctx.prisma.token.delete({
				where: {
					id: token.id,
				},
			})

			if (password !== passwordConfirm)
				return {
					message: 'A senha não corresponde a confirmação de senha',
					success: false,
				}

			if (!user)
				return {
					message: 'Usuário ou senha inválido',
					success: false,
				}

			const salt: string = await bcrypt.genSalt(BCRYPT_SALT)
			const hash: string = await bcrypt.hash(password, salt)

			if (!hash)
				return {
					message:
						'Houve um erro inesperado tente novamente mais tarde',
					success: false,
				}

			await ctx.prisma.user.update({
				data: {
					password: hash,
				},
				where: {
					id: user.id,
				},
			})

			return {
				message: 'A senha foi recuperada com sucesso!',
				success: true,
			}
		}
	}
}
