import 'reflect-metadata'
import { ObjectType, Field, ID, GraphQLTimestamp } from 'type-graphql'
import { User } from '../user/User'

@ObjectType()
export class Profile {
	@Field(() => ID)
	id: number

	@Field()
	fullname: string

	@Field(() => Date)
	birthday?: Date | null

	@Field()
	country: string

	@Field()
	state: string

	@Field()
	city: string

	@Field()
	neighborhood: string

	@Field()
	address: string

	@Field()
	zipcode: string

	@Field()
	cellphone: string

	@Field()
	telephone: string

	@Field()
	allowAutomaticDebit: boolean

	@Field()
	documentation_cpf: string

	@Field()
	documentation_rg: string

	@Field()
	bank_code: string

	@Field()
	bank_agency: string

	@Field()
	bank_account: string

	@Field()
	bank_account_digit: string

	@Field(() => ID)
	userCreatedId: number

	@Field(() => Date)
	createdAt: Date

	@Field(() => ID, { nullable: true })
	userUpdatedId?: number | null

	@Field(() => GraphQLTimestamp, { nullable: true })
	updatedAt?: Date | null

	@Field(() => User, { nullable: true })
	createdBy?: User | null

	@Field(() => User, { nullable: true })
	updatedBy?: User | null
}
