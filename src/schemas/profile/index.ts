import 'reflect-metadata'
import {
	Resolver,
	Query,
	Mutation,
	Arg,
	Ctx,
	FieldResolver,
	Root,
	InputType,
	Field,
	GraphQLTimestamp,
	Int,
} from 'type-graphql'
import { User } from '../user/User'
import { Profile } from './Profile'
import { Context } from '../../config/context'
import { AuthenticationError } from 'apollo-server-express'

@InputType()
class ProfileInput {
	@Field(() => Int, { nullable: true })
	id?: number

	@Field()
	fullname: string

	@Field(() => Date)
	birthday: Date

	@Field()
	country: string

	@Field()
	state: string

	@Field()
	city: string

	@Field()
	neighborhood: string

	@Field()
	address: string

	@Field()
	zipcode: string

	@Field()
	cellphone: string

	@Field()
	telephone: string

	@Field()
	allowAutomaticDebit: boolean

	@Field()
	documentation_cpf: string

	@Field()
	documentation_rg: string

	@Field()
	bank_code: string

	@Field()
	bank_agency: string

	@Field()
	bank_account: string

	@Field()
	bank_account_digit: string

	@Field(() => Int, { nullable: true })
	userCreatedId?: number

	@Field(() => Date, { nullable: true })
	createdAt?: Date

	@Field(() => Int, { nullable: true })
	userUpdatedId?: number | null

	@Field(() => GraphQLTimestamp, { nullable: true })
	updatedAt?: Date | null
}

@InputType()
class PartialProfileInput {
	@Field(() => Int, { nullable: true })
	id?: number

	@Field()
	fullname: string

	@Field()
	city: string

	@Field()
	neighborhood: string

	@Field()
	address: string

	@Field()
	zipcode: string

	@Field()
	cellphone: string

	@Field()
	documentation_cpf: string

	@Field()
	documentation_rg: string

	@Field(() => Int, { nullable: true })
	userCreatedId?: number

	@Field(() => Date, { nullable: true })
	createdAt?: Date

	@Field(() => Int, { nullable: true })
	userUpdatedId?: number | null

	@Field(() => GraphQLTimestamp, { nullable: true })
	updatedAt?: Date | null
}

@Resolver(Profile)
export class ProfileResolver {
	@FieldResolver()
	async createdBy(
		@Root() profile: Profile,
		@Ctx() ctx: Context
	): Promise<User | null> {
		return ctx.prisma.profile
			.findUnique({
				where: {
					id: profile.id,
				},
			})
			.createdBy()
	}

	@FieldResolver()
	async updatedBy(
		@Root() profile: Profile,
		@Ctx() ctx: Context
	): Promise<User | null> {
		return ctx.prisma.profile
			.findUnique({
				where: {
					id: profile.id,
				},
			})
			.updatedBy()
	}

	@Mutation(() => Profile)
	async addProfile(
		@Arg('data') data: ProfileInput,
		@Ctx() ctx: Context
	): Promise<Profile> {
		if (!ctx.user)
			throw new AuthenticationError(
				'Usuário não logado para realizar a operação solicitada!'
			)

		return ctx.prisma.profile.create({
			data: {
				address: data.address,
				bank_account: data.bank_account,
				bank_account_digit: data.bank_account_digit,
				bank_agency: data.bank_agency,
				bank_code: data.bank_code,
				birthday: data.birthday,
				cellphone: data.cellphone,
				city: data.city,
				country: data.country,
				documentation_cpf: data.documentation_cpf,
				documentation_rg: data.documentation_rg,
				fullname: data.fullname,
				neighborhood: data.neighborhood,
				state: data.state,
				telephone: data.telephone,
				zipcode: data.zipcode,
				allowAutomaticDebit: data.allowAutomaticDebit,
				createdAt: data.createdAt,
				createdBy: {
					connect: {
						id: data.userCreatedId
							? data.userCreatedId
							: ctx.user.id,
					},
				},
				user: {
					connect: {
						id: ctx.user.id,
					},
				},
			},
		})
	}

	@Mutation(() => Profile)
	async updateProfile(
		@Arg('data') data: ProfileInput,
		@Ctx() ctx: Context
	): Promise<Profile> {
		if (!ctx.user)
			throw new AuthenticationError(
				'Usuário não logado para realizar a operação solicitada!'
			)

		return ctx.prisma.profile.update({
			data: {
				address: data.address,
				bank_account: data.bank_account,
				bank_account_digit: data.bank_account_digit,
				bank_agency: data.bank_agency,
				bank_code: data.bank_code,
				birthday: data.birthday,
				cellphone: data.cellphone,
				city: data.city,
				country: data.country,
				documentation_cpf: data.documentation_cpf,
				documentation_rg: data.documentation_rg,
				fullname: data.fullname,
				neighborhood: data.neighborhood,
				state: data.state,
				telephone: data.telephone,
				zipcode: data.zipcode,
				allowAutomaticDebit: data.allowAutomaticDebit,
				updatedBy: {
					connect: {
						id: data.userUpdatedId
							? data.userUpdatedId
							: ctx.user.id,
					},
				},
				updatedAt: data.updatedAt,
			},
			where: {
				id: data.id,
			},
		})
	}

	@Mutation(() => Profile)
	async partialUpdateProfile(
		@Arg('data') data: PartialProfileInput,
		@Ctx() ctx: Context
	): Promise<Profile> {
		if (!ctx.user)
			throw new AuthenticationError(
				'Usuário não logado para realizar a operação solicitada!'
			)

		return ctx.prisma.profile.update({
			data: {
				address: data.address,
				cellphone: data.cellphone,
				city: data.city,
				documentation_cpf: data.documentation_cpf,
				documentation_rg: data.documentation_rg,
				fullname: data.fullname,
				neighborhood: data.neighborhood,
				zipcode: data.zipcode,
				updatedBy: {
					connect: {
						id: data.userUpdatedId
							? data.userUpdatedId
							: ctx.user.id,
					},
				},
				updatedAt: data.updatedAt,
			},
			where: {
				id: data.id,
			},
		})
	}

	@Mutation(() => Profile)
	async removeProfile(
		@Arg('id', () => Int) id: number,
		@Ctx() ctx: Context
	): Promise<Profile> {
		if (!ctx.user)
			throw new AuthenticationError(
				'Usuário não logado para realizar a operação solicitada!'
			)

		return ctx.prisma.profile.delete({
			where: {
				id,
			},
		})
	}

	@Query(() => Profile, { nullable: true })
	async profile(
		@Arg('id', () => Int) id: number,
		@Ctx() ctx: Context
	): Promise<Profile | null> {
		return ctx.prisma.profile.findUnique({
			where: { id: id },
		})
	}

	@Query(() => Profile, { nullable: true })
	async profileByUser(@Ctx() ctx: Context): Promise<Profile | null> {
		if (!ctx.user)
			throw new AuthenticationError(
				'Usuário não logado para realizar a operação solicitada!'
			)

		return ctx.prisma.profile.findFirst({
			where: {
				user_id: ctx.user.id,
			},
		})
	}
}
