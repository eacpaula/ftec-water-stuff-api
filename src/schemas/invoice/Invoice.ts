import 'reflect-metadata'
import { ObjectType, Field, ID, GraphQLTimestamp, Int } from 'type-graphql'
import { User } from '../user/User'
import { WaterRegister } from '../water-register/WaterRegister'

@ObjectType()
export class Invoice {
	@Field(() => ID)
	id: number

	@Field(() => ID, { nullable: true })
	user_id?: number | null

	@Field(() => ID)
	water_register_id: number

	@Field()
	total: number

	@Field(() => Int)
	status: number

	@Field()
	barcode: string

	@Field(() => ID)
	userCreatedId: number

	@Field(() => Date)
	createdAt: Date

	@Field(() => ID, { nullable: true })
	userUpdatedId?: number | null

	@Field(() => GraphQLTimestamp, { nullable: true })
	updatedAt?: Date | null

	@Field(() => User, { nullable: true })
	createdBy?: User | null

	@Field(() => User, { nullable: true })
	updatedBy?: User | null

	@Field(() => User, { nullable: true })
	user?: User | null

	@Field(() => WaterRegister, { nullable: true })
	waterRegister?: WaterRegister | null
}

export enum InvoiceStatus {
	PAYED = 0,
	PAYABLE = 1,
}
