import 'reflect-metadata'
import {
	Resolver,
	Query,
	Mutation,
	Arg,
	Ctx,
	FieldResolver,
	Root,
	InputType,
	Field,
	GraphQLTimestamp,
	Int,
} from 'type-graphql'
import { User } from '../user/User'
import { Invoice, InvoiceStatus } from './Invoice'
import { Context } from '../../config/context'
import SearchInput from '../common/SearchInput'
import { AuthenticationError } from 'apollo-server-express'
import { WaterRegister } from '../water-register/WaterRegister'

@InputType()
class InvoiceInput {
	@Field(() => Int, { nullable: true })
	id?: number

	@Field(() => Int, { nullable: true })
	user_id?: number | null

	@Field(() => Int)
	water_register_id: number

	@Field()
	total: number

	@Field(() => Int)
	status: number

	@Field()
	barcode: string

	@Field(() => Int, { nullable: true })
	userCreatedId?: number

	@Field(() => Date, { nullable: true })
	createdAt?: Date

	@Field(() => Int, { nullable: true })
	userUpdatedId?: number | null

	@Field(() => GraphQLTimestamp, { nullable: true })
	updatedAt?: Date | null
}

@Resolver(Invoice)
export class InvoiceResolver {
	@FieldResolver()
	async createdBy(
		@Root() invoice: Invoice,
		@Ctx() ctx: Context
	): Promise<User | null> {
		return ctx.prisma.invoice
			.findUnique({
				where: {
					id: invoice.id,
				},
			})
			.createdBy()
	}

	@FieldResolver()
	async updatedBy(
		@Root() invoice: Invoice,
		@Ctx() ctx: Context
	): Promise<User | null> {
		return ctx.prisma.invoice
			.findUnique({
				where: {
					id: invoice.id,
				},
			})
			.updatedBy()
	}

	@FieldResolver()
	async user(
		@Root() invoice: Invoice,
		@Ctx() ctx: Context
	): Promise<User | null> {
		return ctx.prisma.invoice
			.findUnique({
				where: {
					id: invoice.id,
				},
			})
			.user()
	}

	@FieldResolver()
	async waterRegister(
		@Root() invoice: Invoice,
		@Ctx() ctx: Context
	): Promise<WaterRegister | null> {
		return ctx.prisma.waterRegisterContact
			.findUnique({
				where: {
					id: invoice.id,
				},
			})
			.waterRegister()
	}

	@Mutation(() => Invoice)
	async addInvoice(
		@Arg('data') data: InvoiceInput,
		@Ctx() ctx: Context
	): Promise<Invoice> {
		if (!ctx.user)
			throw new AuthenticationError(
				'Usuário não logado para realizar a operação solicitada!'
			)

		return ctx.prisma.invoice.create({
			data: {
				barcode: data.barcode,
				total: data.total,
				status: data.status,
				waterRegister: {
					connect: {
						id: data.water_register_id,
					},
				},
				createdBy: {
					connect: {
						id: data.userCreatedId
							? data.userCreatedId
							: ctx.user.id,
					},
				},
				user: {
					connect: {
						id: data.user_id ? data.user_id : ctx.user.id,
					},
				},
				createdAt: data.createdAt,
			},
		})
	}

	@Mutation(() => Invoice)
	async updateInvoice(
		@Arg('data') data: InvoiceInput,
		@Ctx() ctx: Context
	): Promise<Invoice> {
		if (!ctx.user)
			throw new AuthenticationError(
				'Usuário não logado para realizar a operação solicitada!'
			)

		return ctx.prisma.invoice.update({
			data: {
				barcode: data.barcode,
				total: data.total,
				status: data.status,
				waterRegister: {
					connect: {
						id: data.water_register_id,
					},
				},
				user: {
					connect: {
						id: data.user_id ? data.user_id : ctx.user.id,
					},
				},
				updatedBy: {
					connect: {
						id: data.userUpdatedId
							? data.userUpdatedId
							: ctx.user.id,
					},
				},
				updatedAt: data.updatedAt,
			},
			where: {
				id: data.id,
			},
		})
	}

	@Mutation(() => Invoice)
	async removeInvoice(
		@Arg('id', () => Int) id: number,
		@Ctx() ctx: Context
	): Promise<Invoice> {
		if (!ctx.user)
			throw new AuthenticationError(
				'Usuário não logado para realizar a operação solicitada!'
			)

		return ctx.prisma.invoice.delete({
			where: {
				id,
			},
		})
	}

	@Query(() => Invoice, { nullable: true })
	async invoice(
		@Arg('id', () => Int) id: number,
		@Ctx() ctx: Context
	): Promise<Invoice | null> {
		return ctx.prisma.invoice.findUnique({
			where: { id: id },
		})
	}

	@Query(() => [Invoice], { nullable: true })
	async invoices(
		@Arg('params') params: SearchInput,
		@Ctx() ctx: Context
	): Promise<Invoice[]> {
		if (!params.skip && !params.take) return ctx.prisma.invoice.findMany()

		return ctx.prisma.invoice.findMany({
			skip: params.skip,
			take: params.take,
		})
	}

	@Query(() => [Invoice], { nullable: true })
	async invoicesByUser(
		@Arg('params') params: SearchInput,
		@Ctx() ctx: Context
	): Promise<Invoice[]> {
		if (!ctx.user)
			throw new AuthenticationError(
				'Usuário não logado para realizar a operação solicitada!'
			)

		const waterRegister = await ctx.prisma.waterRegister.findFirst({
			where: { user_id: ctx.user.id },
		})

		if (!params.skip && !params.take)
			return ctx.prisma.invoice.findMany({
				where: {
					water_register_id: waterRegister?.id,
				},
			})

		return ctx.prisma.invoice.findMany({
			skip: params.skip,
			take: params.take,
			where: {
				water_register_id: waterRegister?.id,
			},
		})
	}

	@Query(() => [Invoice], { nullable: true })
	async pendantInvoicesByUser(
		@Arg('params') params: SearchInput,
		@Ctx() ctx: Context
	): Promise<Invoice | null> {
		if (!ctx.user)
			throw new AuthenticationError(
				'Usuário não logado para realizar a operação solicitada!'
			)

		return await ctx.prisma.invoice.findFirst({
			where: {
				user_id: ctx.user.id,
				status: 1,
			},
		})
	}
}
