import 'reflect-metadata'
import {
	Resolver,
	Arg,
	Ctx,
	InputType,
	Field,
	Query,
	ObjectType,
	Mutation,
} from 'type-graphql'
import { Context } from '../../config/context'
import { AuthenticationError } from 'apollo-server-express'
import sendEmail from '../../helpers/sendEmail'
import DefaultResponse from '../common/DefaultReponse'

@InputType()
class AutomaticDebitInput {
	@Field()
	bank: string

	@Field()
	agency: string

	@Field()
	account: string

	@Field()
	account_digit: string

	@Field()
	documentation: string

	@Field()
	fullname: string

	@Field()
	email: string

	@Field()
	cellphone: string
}

@ObjectType()
class CustomDefaultResponse extends DefaultResponse {
	@Field({ nullable: true })
	enableRequest?: boolean
}

@Resolver(Object)
export class AutomaticDebitResolver {
	@Query(() => CustomDefaultResponse, { nullable: true })
	async checkStatusAutomaticDebit(
		@Ctx() ctx: Context
	): Promise<CustomDefaultResponse> {
		if (!ctx.user)
			return {
				message:
					'O usuário não tem acesso para utilizar o recurso solicitado!',
				enableRequest: false,
				success: false,
			}

		const profile = await ctx.prisma.profile.findFirst({
			where: { user_id: ctx.user.id },
		})

		if (!profile)
			return {
				message: 'O usuário não possui o perfil atualizado!',
				enableRequest: true,
				success: true,
			}

		if (
			profile.bank_account &&
			profile.bank_account_digit &&
			profile.bank_agency &&
			profile.bank_code
		)
			return {
				message: 'Solicitação ja foi realizada',
				enableRequest: false,
				success: false,
			}

		return {
			message: profile.allowAutomaticDebit
				? 'Débito automático aprovado!'
				: 'Débito automático em aprovação!',
			enableRequest: false,
			success: true,
		}
	}

	@Mutation(() => DefaultResponse, { nullable: true })
	async automaticDebit(
		@Arg('data') data: AutomaticDebitInput,
		@Ctx() ctx: Context
	): Promise<DefaultResponse> {
		if (!ctx.user)
			throw new AuthenticationError(
				'Usuário não logado para realizar a operação solicitada!'
			)

		const profile = await ctx.prisma.profile.findFirst({
			where: {
				user_id: ctx.user.id,
			},
		})

		if (profile)
			await ctx.prisma.profile.update({
				data: {
					bank_account: data.account,
					bank_account_digit: data.account_digit,
					bank_agency: data.agency,
					bank_code: data.bank,
					cellphone: data.cellphone,
					documentation_rg: data.documentation,
					fullname: data.fullname,
					updatedBy: {
						connect: {
							id: ctx.user.id,
						},
					},
					updatedAt: new Date(),
				},
				where: {
					id: profile.id,
				},
			})
		else
			await ctx.prisma.profile.create({
				data: {
					address: '',
					bank_account: data.account,
					bank_account_digit: data.account_digit,
					bank_agency: data.agency,
					bank_code: data.bank,
					birthday: null,
					cellphone: data.cellphone,
					city: '',
					country: '',
					documentation_cpf: '',
					documentation_rg: data.documentation,
					fullname: data.fullname,
					neighborhood: '',
					state: '',
					telephone: '',
					zipcode: '',
					allowAutomaticDebit: false,
					createdBy: {
						connect: {
							id: ctx.user.id,
						},
					},
					createdAt: new Date(),
					user: {
						connect: {
							id: ctx.user.id,
						},
					},
				},
			})

		const subject = await ctx.prisma.subject.findUnique({
			where: { id: 4 },
		})

		if (subject)
			await sendEmail(
				`Solicitação de Contato: ${subject.title}`,
				`
					<p>
						Nome: ${data.fullname}, <br/>
						Email: ${data.email}, <br/>
						Celular: ${data.cellphone}, <br/>
					</p>
				`,
				subject.emails.split(',')
			)

		return {
			success: true,
			message:
				'Solicitação de cadastro de débito automático realizada com sucesso!',
		}
	}
}
