import 'reflect-metadata'
import {
	Resolver,
	Query,
	Mutation,
	Arg,
	Ctx,
	FieldResolver,
	Root,
	InputType,
	Field,
	Int,
	GraphQLTimestamp,
} from 'type-graphql'
import { User } from '../user/User'
import { WaterRegisterHistory } from './WaterRegisterHistory'
import { Context } from '../../config/context'
import SearchInput from '../common/SearchInput'
import { AuthenticationError } from 'apollo-server-express'
import { WaterRegister } from '../water-register/WaterRegister'

@InputType()
class WaterRegisterHistoryInput {
	@Field(() => Int, { nullable: true })
	id?: number

	@Field(() => Int)
	water_register_id: number

	@Field()
	consumption: number

	@Field(() => Int, { nullable: true })
	userCreatedId?: number

	@Field(() => Date, { nullable: true })
	createdAt?: Date
}

@InputType()
class CustomSearchInput extends SearchInput {}

@Resolver(WaterRegisterHistory)
export class WaterRegisterHistoryResolver {
	@FieldResolver()
	async waterRegister(
		@Root() waterRegisterHistory: WaterRegisterHistory,
		@Ctx() ctx: Context
	): Promise<WaterRegister | null> {
		return ctx.prisma.waterRegisterHistory
			.findUnique({
				where: {
					id: waterRegisterHistory.id,
				},
			})
			.waterRegister()
	}

	@FieldResolver()
	async createdBy(
		@Root() waterRegisterHistory: WaterRegisterHistory,
		@Ctx() ctx: Context
	): Promise<User | null> {
		return ctx.prisma.waterRegisterHistory
			.findUnique({
				where: {
					id: waterRegisterHistory.id,
				},
			})
			.createdBy()
	}

	@Mutation(() => WaterRegisterHistory)
	async addWaterRegisterHistory(
		@Arg('data') data: WaterRegisterHistoryInput,
		@Ctx() ctx: Context
	): Promise<WaterRegisterHistory> {
		if (!ctx.user)
			throw new AuthenticationError(
				'Usuário não logado para realizar a operação solicitada!'
			)

		return ctx.prisma.waterRegisterHistory.create({
			data: {
				consumption: data.consumption,
				waterRegister: {
					connect: {
						id: data.water_register_id,
					},
				},
				createdBy: {
					connect: {
						id: data.userCreatedId
							? data.userCreatedId
							: ctx.user.id,
					},
				},
				createdAt: data.createdAt,
			},
		})
	}

	@Mutation(() => WaterRegisterHistory)
	async updateWaterRegisterHistory(
		@Arg('data') data: WaterRegisterHistoryInput,
		@Ctx() ctx: Context
	): Promise<WaterRegisterHistory> {
		if (!ctx.user)
			throw new AuthenticationError(
				'Usuário não logado para realizar a operação solicitada!'
			)

		return ctx.prisma.waterRegisterHistory.update({
			data: {
				consumption: data.consumption,
				waterRegister: {
					connect: {
						id: data.water_register_id,
					},
				},
			},
			where: {
				id: data.id,
			},
		})
	}

	@Mutation(() => WaterRegisterHistory)
	async removeWaterRegisterHistory(
		@Arg('id', () => Int) id: number,
		@Ctx() ctx: Context
	): Promise<WaterRegisterHistory> {
		if (!ctx.user)
			throw new AuthenticationError(
				'Usuário não logado para realizar a operação solicitada!'
			)

		return ctx.prisma.waterRegisterHistory.delete({
			where: {
				id,
			},
		})
	}

	@Query(() => WaterRegisterHistory, { nullable: true })
	async waterRegisterHistory(
		@Arg('id', () => Int) id: number,
		@Ctx() ctx: Context
	): Promise<WaterRegisterHistory | null> {
		return ctx.prisma.waterRegisterHistory.findUnique({
			where: { id: id },
		})
	}

	@Query(() => [WaterRegisterHistory], { nullable: true })
	async waterRegisterHistoriesByUser(
		@Arg('params') params: CustomSearchInput,
		@Ctx() ctx: Context
	): Promise<WaterRegisterHistory[]> {
		if (!ctx.user)
			throw new AuthenticationError(
				'Usuário não logado para realizar a operação solicitada!'
			)

		const waterRegister = await ctx.prisma.waterRegister.findFirst({
			where: { user_id: ctx.user.id },
		})

		if (!params.skip && !params.take)
			return ctx.prisma.waterRegisterHistory.findMany({
				where: {
					water_register_id: waterRegister?.id,
				},
			})

		return ctx.prisma.waterRegisterHistory.findMany({
			skip: params.skip,
			take: params.take,
			where: {
				water_register_id: waterRegister?.id,
			},
		})
	}

	@Query(() => [WaterRegisterHistory], { nullable: true })
	async waterRegisterHistories(
		@Arg('params') params: SearchInput,
		@Ctx() ctx: Context
	): Promise<WaterRegisterHistory[]> {
		if (!params.skip && !params.take)
			return ctx.prisma.waterRegisterHistory.findMany()

		return ctx.prisma.waterRegisterHistory.findMany({
			skip: params.skip,
			take: params.take,
		})
	}
}
