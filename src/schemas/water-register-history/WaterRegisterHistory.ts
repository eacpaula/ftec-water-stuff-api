import 'reflect-metadata'
import { ObjectType, Field, ID } from 'type-graphql'
import { User } from '../user/User'
import { WaterRegister } from '../water-register/WaterRegister'

@ObjectType()
export class WaterRegisterHistory {
	@Field(() => ID)
	id: number

	@Field(() => ID)
	water_register_id: number

	@Field()
	consumption: number

	@Field(() => ID)
	userCreatedId: number

	@Field(() => Date)
	createdAt: Date

	@Field(() => User, { nullable: true })
	createdBy?: User | null

	@Field(() => WaterRegister, { nullable: true })
	waterRegister?: WaterRegister | null
}
