import 'reflect-metadata'
import { ObjectType, Field, ID, GraphQLTimestamp } from 'type-graphql'
import { Contact } from '../contact/Contact'
import { User } from '../user/User'
import { WaterRegister } from '../water-register/WaterRegister'

@ObjectType()
export class WaterRegisterContact {
	@Field(() => ID)
	id: number

	@Field(() => ID)
	water_register_id: number

	@Field()
	contact_id: number

	@Field(() => ID)
	userCreatedId: number

	@Field(() => Date)
	createdAt: Date

	@Field(() => ID, { nullable: true })
	userUpdatedId?: number | null

	@Field(() => GraphQLTimestamp, { nullable: true })
	updatedAt?: Date | null

	@Field(() => Contact, { nullable: true })
	contact?: Contact | null

	@Field(() => WaterRegister, { nullable: true })
	waterRegister?: WaterRegister | null

	@Field(() => User, { nullable: true })
	createdBy?: User | null

	@Field(() => User, { nullable: true })
	updatedBy?: User | null
}
