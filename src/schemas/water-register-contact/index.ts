import 'reflect-metadata'
import {
	Resolver,
	Query,
	Mutation,
	Arg,
	Ctx,
	FieldResolver,
	Root,
	InputType,
	Field,
	Int,
	GraphQLTimestamp,
} from 'type-graphql'
import { User } from '../user/User'
import { WaterRegisterContact } from './WaterRegisterContact'
import { Context } from '../../config/context'
import SearchInput from '../common/SearchInput'
import { AuthenticationError } from 'apollo-server-express'
import { WaterRegister } from '../water-register/WaterRegister'

@InputType()
class WaterRegisterContactInput {
	@Field(() => Int)
	water_register_id: number

	@Field(() => Int, { nullable: true })
	subject_id?: number

	@Field()
	message: string
}

@Resolver(WaterRegisterContact)
export class WaterRegisterContactResolver {
	@FieldResolver()
	async waterRegister(
		@Root() waterRegisterContact: WaterRegisterContact,
		@Ctx() ctx: Context
	): Promise<WaterRegister | null> {
		return ctx.prisma.waterRegisterContact
			.findUnique({
				where: {
					id: waterRegisterContact.id,
				},
			})
			.waterRegister()
	}

	@FieldResolver()
	async createdBy(
		@Root() WaterRegisterContact: WaterRegisterContact,
		@Ctx() ctx: Context
	): Promise<User | null> {
		return ctx.prisma.waterRegisterContact
			.findUnique({
				where: {
					id: WaterRegisterContact.id,
				},
			})
			.createdBy()
	}

	@FieldResolver()
	async updatedBy(
		@Root() waterRegisterContact: WaterRegisterContact,
		@Ctx() ctx: Context
	): Promise<User | null> {
		return ctx.prisma.waterRegisterContact
			.findUnique({
				where: {
					id: waterRegisterContact.id,
				},
			})
			.updatedBy()
	}

	@Mutation(() => WaterRegisterContact)
	async addWaterRegisterContact(
		@Arg('data') data: WaterRegisterContactInput,
		@Ctx() ctx: Context
	): Promise<WaterRegisterContact> {
		if (!ctx.user)
			throw new AuthenticationError(
				'Usuário não logado para realizar a operação solicitada!'
			)

		const user = await ctx.prisma.user.findFirst({
			where: { id: ctx.user.id },
		})

		const profile = await ctx.prisma.profile.findFirst({
			where: { user_id: ctx.user.id },
		})

		const waterRegister = await ctx.prisma.waterRegister.findFirst({
			where: { id: data.water_register_id },
		})

		const contact = await ctx.prisma.contact.create({
			data: {
				status: 1,
				fullname: profile?.fullname || '',
				email: user?.email || '',
				cellphone: profile?.cellphone || waterRegister?.cellphone || '',
				message: data.message,
				subject: {
					connect: {
						id: data.subject_id ? data.subject_id : 3,
					},
				},
				createdAt: new Date(),
			},
		})

		return ctx.prisma.waterRegisterContact.create({
			data: {
				waterRegister: {
					connect: {
						id: data.water_register_id,
					},
				},
				contact: {
					connect: {
						id: contact.id,
					},
				},
				createdBy: {
					connect: {
						id: ctx.user.id,
					},
				},
				createdAt: new Date(),
			},
		})
	}

	@Query(() => WaterRegisterContact, { nullable: true })
	async waterRegisterContact(
		@Arg('id', () => Int) id: number,
		@Ctx() ctx: Context
	): Promise<WaterRegisterContact | null> {
		return ctx.prisma.waterRegisterContact.findUnique({
			where: { id: id },
		})
	}

	@Query(() => [WaterRegisterContact], { nullable: true })
	async waterRegisterContacts(
		@Arg('params') params: SearchInput,
		@Ctx() ctx: Context
	): Promise<WaterRegisterContact[]> {
		if (!params.skip && !params.take)
			return ctx.prisma.waterRegisterContact.findMany()

		return ctx.prisma.waterRegisterContact.findMany({
			skip: params.skip,
			take: params.take,
		})
	}
}
