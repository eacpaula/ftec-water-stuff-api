import 'reflect-metadata'
import {
	Resolver,
	Query,
	Mutation,
	Arg,
	Ctx,
	FieldResolver,
	Root,
	InputType,
	Field,
	Int,
} from 'type-graphql'
import { User } from '../user/User'
import { Complaint } from './Complaint'
import { Context } from '../../config/context'
import SearchInput from '../common/SearchInput'
import { AuthenticationError } from 'apollo-server-express'

@InputType()
class ComplaintInput {
	@Field(() => String)
	title: string

	@Field(() => String)
	subtitle: string

	@Field(() => String)
	description: string
}

@Resolver(Complaint)
export class ComplaintResolver {
	@FieldResolver()
	async createdBy(
		@Root() complaint: Complaint,
		@Ctx() ctx: Context
	): Promise<User | null> {
		return ctx.prisma.complaint
			.findUnique({
				where: {
					id: complaint.id,
				},
			})
			.createdBy()
	}

	@Mutation(() => Complaint)
	async addComplaint(
		@Arg('data') data: ComplaintInput,
		@Ctx() ctx: Context
	): Promise<Complaint> {
		if (!ctx.user)
			throw new AuthenticationError(
				'Usuário não logado para realizar a operação solicitada!'
			)

		return ctx.prisma.complaint.create({
			data: {
				title: data.title,
				subtitle: data.subtitle,
				description: data.description,
				createdBy: {
					connect: {
						id: ctx.user.id,
					},
				},
				createdAt: new Date(),
			},
		})
	}

	@Query(() => Complaint, { nullable: true })
	async complaint(
		@Arg('id', () => Int) id: number,
		@Ctx() ctx: Context
	): Promise<Complaint | null> {
		return ctx.prisma.complaint.findUnique({
			where: { id: id },
		})
	}

	@Query(() => [Complaint], { nullable: true })
	async complaints(
		@Arg('params') params: SearchInput,
		@Ctx() ctx: Context
	): Promise<Complaint[]> {
		if (!params.skip && !params.take) return ctx.prisma.complaint.findMany()

		return ctx.prisma.complaint.findMany({
			skip: params.skip,
			take: params.take,
		})
	}

	@Query(() => [Complaint], { nullable: true })
	async complaintsByUser(
		@Arg('params') params: SearchInput,
		@Ctx() ctx: Context
	): Promise<Complaint[]> {
		if (!params.skip && !params.take) return ctx.prisma.complaint.findMany()

		return ctx.prisma.complaint.findMany({
			skip: params.skip,
			take: params.take,
			where: {
				userCreatedId: ctx.user.id,
			},
		})
	}
}
