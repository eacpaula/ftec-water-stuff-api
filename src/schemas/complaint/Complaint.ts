import 'reflect-metadata'
import { ObjectType, Field, ID } from 'type-graphql'
import { User } from '../user/User'

@ObjectType()
export class Complaint {
	@Field(() => ID)
	id: number

	@Field(() => String)
	title: string

	@Field(() => String)
	subtitle: string

	@Field(() => String)
	description: string

	@Field(() => ID)
	userCreatedId: number

	@Field(() => Date)
	createdAt: Date

	@Field(() => User, { nullable: true })
	createdBy?: User | null
}
