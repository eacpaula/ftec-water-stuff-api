import * as tq from 'type-graphql'

import { BannerResolver } from './banner'
import { ContactResolver } from './contact'
import { MediaResolver } from './media'
import { NewsResolver } from './news'
import { SubjectResolver } from './subject'
import { UserResolver } from './user'
import { ProfileResolver } from './profile'
import { WaterRegisterResolver } from './water-register'
import { WaterRegisterContactResolver } from './water-register-contact'
import { WaterRegisterHistoryResolver } from './water-register-history'
import { InvoiceResolver } from './invoice'
import { AssessmentResolver } from './assessment'
import { AutomaticDebitResolver } from './automatic-debit'
import { RatingResolver } from './rating'
import { ComplaintResolver } from './complaint'

export const schema = tq.buildSchemaSync({
	resolvers: [
		BannerResolver,
		ContactResolver,
		MediaResolver,
		NewsResolver,
		SubjectResolver,
		UserResolver,
		ProfileResolver,
		WaterRegisterResolver,
		WaterRegisterContactResolver,
		WaterRegisterHistoryResolver,
		InvoiceResolver,
		AssessmentResolver,
		AutomaticDebitResolver,
		RatingResolver,
		ComplaintResolver,
	],
})
