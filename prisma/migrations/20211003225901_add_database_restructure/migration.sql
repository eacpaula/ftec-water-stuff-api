/*
  Warnings:

  - The `status` column on the `Invoice` table would be dropped and recreated. This will lead to data loss if there is data in the column.
  - Added the required column `level` to the `Assessments` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Assessments" DROP COLUMN "level",
ADD COLUMN     "level" INTEGER NOT NULL;

-- AlterTable
ALTER TABLE "Invoice" DROP COLUMN "status",
ADD COLUMN     "status" INTEGER NOT NULL DEFAULT 0;

-- DropEnum
DROP TYPE "AssessmentsLevel";

-- DropEnum
DROP TYPE "InvoiceStatus";
