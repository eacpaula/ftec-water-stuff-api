/*
  Warnings:

  - You are about to drop the `Area` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Client` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Product` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Proposal` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `WorkWithUs` table. If the table is not empty, all the data it contains will be lost.

*/
-- CreateEnum
CREATE TYPE "InvoiceStatus" AS ENUM ('PAYED', 'PAYABLE');

-- CreateEnum
CREATE TYPE "AssessmentsLevel" AS ENUM ('VERY_UNSATISFIED', 'UNSATISFIED', 'SATISFIED', 'VERY_SATISFIED', 'EXCELLENT');

-- AlterEnum
ALTER TYPE "Role" ADD VALUE 'CLERK';

-- DropForeignKey
ALTER TABLE "Area" DROP CONSTRAINT "Area_userCreatedId_fkey";

-- DropForeignKey
ALTER TABLE "Area" DROP CONSTRAINT "Area_userUpdatedId_fkey";

-- DropForeignKey
ALTER TABLE "Client" DROP CONSTRAINT "Client_media_id_fkey";

-- DropForeignKey
ALTER TABLE "Client" DROP CONSTRAINT "Client_userCreatedId_fkey";

-- DropForeignKey
ALTER TABLE "Client" DROP CONSTRAINT "Client_userUpdatedId_fkey";

-- DropForeignKey
ALTER TABLE "Product" DROP CONSTRAINT "Product_media_id_fkey";

-- DropForeignKey
ALTER TABLE "Product" DROP CONSTRAINT "Product_userCreatedId_fkey";

-- DropForeignKey
ALTER TABLE "Product" DROP CONSTRAINT "Product_userUpdatedId_fkey";

-- DropForeignKey
ALTER TABLE "Proposal" DROP CONSTRAINT "Proposal_product_id_fkey";

-- DropForeignKey
ALTER TABLE "WorkWithUs" DROP CONSTRAINT "WorkWithUs_area_id_fkey";

-- DropForeignKey
ALTER TABLE "WorkWithUs" DROP CONSTRAINT "WorkWithUs_media_id_fkey";

-- AlterTable
ALTER TABLE "User" ADD COLUMN     "available" BOOLEAN NOT NULL DEFAULT true;

-- DropTable
DROP TABLE "Area";

-- DropTable
DROP TABLE "Client";

-- DropTable
DROP TABLE "Product";

-- DropTable
DROP TABLE "Proposal";

-- DropTable
DROP TABLE "WorkWithUs";

-- CreateTable
CREATE TABLE "Profile" (
    "id" SERIAL NOT NULL,
    "user_id" INTEGER NOT NULL,
    "fullname" VARCHAR(256) NOT NULL,
    "birthday" TIMESTAMP NOT NULL,
    "country" VARCHAR(256) NOT NULL,
    "state" VARCHAR(256) NOT NULL,
    "city" VARCHAR(256) NOT NULL,
    "neighborhood" VARCHAR(256) NOT NULL,
    "address" VARCHAR(256) NOT NULL,
    "zipcode" VARCHAR(50) NOT NULL,
    "cellphone" VARCHAR(50) NOT NULL,
    "telephone" VARCHAR(50) NOT NULL,
    "allowAutomaticDebit" BOOLEAN NOT NULL DEFAULT false,
    "documentation_cpf" VARCHAR(25) NOT NULL,
    "documentation_rg" VARCHAR(25) NOT NULL,
    "bank_code" VARCHAR(25) NOT NULL,
    "bank_agency" VARCHAR(25) NOT NULL,
    "bank_account" VARCHAR(30) NOT NULL,
    "bank_account_digit" VARCHAR(10) NOT NULL,
    "userCreatedId" INTEGER NOT NULL,
    "createdAt" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "userUpdatedId" INTEGER,
    "updatedAt" TIMESTAMP,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "WaterRegister" (
    "id" SERIAL NOT NULL,
    "user_id" INTEGER NOT NULL,
    "code" VARCHAR(256) NOT NULL,
    "documentation" VARCHAR(256) NOT NULL,
    "media_user_id" INTEGER NOT NULL,
    "media_documentation_frontside_id" INTEGER NOT NULL,
    "media_documentation_backside_id" INTEGER NOT NULL,
    "media_water_account_id" INTEGER NOT NULL,
    "accept_terms" BOOLEAN NOT NULL DEFAULT false,
    "available" BOOLEAN NOT NULL DEFAULT true,
    "userCreatedId" INTEGER NOT NULL,
    "createdAt" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "userUpdatedId" INTEGER,
    "updatedAt" TIMESTAMP,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "WaterRegisterHistory" (
    "id" SERIAL NOT NULL,
    "water_register_id" INTEGER NOT NULL,
    "consumption" DOUBLE PRECISION NOT NULL,
    "userCreatedId" INTEGER NOT NULL,
    "createdAt" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "WaterRegisterContact" (
    "id" SERIAL NOT NULL,
    "water_register_id" INTEGER NOT NULL,
    "contact_id" INTEGER NOT NULL,
    "userCreatedId" INTEGER NOT NULL,
    "createdAt" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "userUpdatedId" INTEGER,
    "updatedAt" TIMESTAMP,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Invoice" (
    "id" SERIAL NOT NULL,
    "user_id" INTEGER NOT NULL,
    "water_register_id" INTEGER NOT NULL,
    "total" DOUBLE PRECISION NOT NULL,
    "status" "InvoiceStatus" DEFAULT E'PAYABLE',
    "barcode" VARCHAR(256) NOT NULL,
    "userCreatedId" INTEGER NOT NULL,
    "createdAt" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "userUpdatedId" INTEGER,
    "updatedAt" TIMESTAMP,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Assessments" (
    "id" SERIAL NOT NULL,
    "contact_id" INTEGER NOT NULL,
    "level" "InvoiceStatus" DEFAULT E'PAYABLE',
    "observation" VARCHAR(2000) NOT NULL,
    "userCreatedId" INTEGER NOT NULL,
    "createdAt" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

    PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "Profile" ADD FOREIGN KEY ("user_id") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Profile" ADD FOREIGN KEY ("userCreatedId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Profile" ADD FOREIGN KEY ("userUpdatedId") REFERENCES "User"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "WaterRegister" ADD FOREIGN KEY ("media_user_id") REFERENCES "Media"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "WaterRegister" ADD FOREIGN KEY ("media_documentation_frontside_id") REFERENCES "Media"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "WaterRegister" ADD FOREIGN KEY ("media_documentation_backside_id") REFERENCES "Media"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "WaterRegister" ADD FOREIGN KEY ("media_water_account_id") REFERENCES "Media"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "WaterRegister" ADD FOREIGN KEY ("userCreatedId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "WaterRegister" ADD FOREIGN KEY ("userUpdatedId") REFERENCES "User"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "WaterRegisterHistory" ADD FOREIGN KEY ("userCreatedId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "WaterRegisterHistory" ADD FOREIGN KEY ("water_register_id") REFERENCES "WaterRegister"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "WaterRegisterContact" ADD FOREIGN KEY ("water_register_id") REFERENCES "WaterRegister"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "WaterRegisterContact" ADD FOREIGN KEY ("contact_id") REFERENCES "Contact"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "WaterRegisterContact" ADD FOREIGN KEY ("userUpdatedId") REFERENCES "User"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "WaterRegisterContact" ADD FOREIGN KEY ("userCreatedId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Invoice" ADD FOREIGN KEY ("water_register_id") REFERENCES "WaterRegister"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Invoice" ADD FOREIGN KEY ("userCreatedId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Invoice" ADD FOREIGN KEY ("userUpdatedId") REFERENCES "User"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Assessments" ADD FOREIGN KEY ("userCreatedId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Assessments" ADD FOREIGN KEY ("contact_id") REFERENCES "Contact"("id") ON DELETE CASCADE ON UPDATE CASCADE;
