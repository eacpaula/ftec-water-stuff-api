/*
  Warnings:

  - Added the required column `address` to the `WaterRegister` table without a default value. This is not possible if the table is not empty.
  - Added the required column `cellphone` to the `WaterRegister` table without a default value. This is not possible if the table is not empty.
  - Added the required column `city` to the `WaterRegister` table without a default value. This is not possible if the table is not empty.
  - Added the required column `country` to the `WaterRegister` table without a default value. This is not possible if the table is not empty.
  - Added the required column `neighborhood` to the `WaterRegister` table without a default value. This is not possible if the table is not empty.
  - Added the required column `state` to the `WaterRegister` table without a default value. This is not possible if the table is not empty.
  - Added the required column `telephone` to the `WaterRegister` table without a default value. This is not possible if the table is not empty.
  - Added the required column `zipcode` to the `WaterRegister` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "WaterRegister" ADD COLUMN     "address" VARCHAR(256) NOT NULL,
ADD COLUMN     "cellphone" VARCHAR(50) NOT NULL,
ADD COLUMN     "city" VARCHAR(256) NOT NULL,
ADD COLUMN     "country" VARCHAR(256) NOT NULL,
ADD COLUMN     "neighborhood" VARCHAR(256) NOT NULL,
ADD COLUMN     "state" VARCHAR(256) NOT NULL,
ADD COLUMN     "telephone" VARCHAR(50) NOT NULL,
ADD COLUMN     "zipcode" VARCHAR(50) NOT NULL;
